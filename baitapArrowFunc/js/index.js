const colorLists = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let renderButton = (list) => {
  let contenHTML = "";
  list.forEach((item) => {
    let content = `<butoon class="color-button ${item}"></butoon>`;
    contenHTML += content;
  });

  document.getElementById("colorContainer").innerHTML = contenHTML;
};

renderButton(colorLists);

let buttonLists = document.querySelectorAll(".color-button");

buttonLists[0].classList.add("active");

document.getElementById("house").classList.add(colorLists[0]);

buttonLists.forEach((button, indexButton) => {
  button.onclick = function () {
    document.querySelector(".color-button.active").classList.remove("active");
    this.classList.add("active");

    colorLists.forEach((item, indexColor) => {
      if (indexButton == indexColor) {
        document.getElementById("house").classList.add(item);
      } else {
        document.getElementById("house").classList.remove(item);
      }
    });
  };
});
