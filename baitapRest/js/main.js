let tinhDTB = (...arr) => {
  let sum = 0;
  arr.forEach((item) => {
    sum += item;
  });
  return (sum / arr.length).toFixed(2);
};

function diemTBKhoi1() {
  let diemToan = document.getElementById("inpToan").value * 1;
  let diemLy = document.getElementById("inpLy").value * 1;
  let diemHoa = document.getElementById("inpHoa").value * 1;
  let diemTB = tinhDTB(diemToan, diemLy, diemHoa);
  document.getElementById("tbKhoi1").innerText = diemTB;
}

let diemTBKhoi2 = () => {
  let diemVan = document.getElementById("inpVan").value * 1;
  let diemSu = document.getElementById("inpSu").value * 1;
  let diemDia = document.getElementById("inpDia").value * 1;
  let diemAnh = document.getElementById("inpEnglish").value * 1;
  let diemTB = tinhDTB(diemVan, diemSu, diemDia, diemAnh);
  document.getElementById("tbKhoi2").innerText = diemTB;
};
